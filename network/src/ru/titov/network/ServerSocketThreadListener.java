package ru.titov.network;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by valentintitov on 29.06.17.
 */
public interface ServerSocketThreadListener {

    void onStartServerSocketThread(ServerSocketThread thread);
    void onStopServerSocketThread(ServerSocketThread thread);

    void onReadyServerSocketThread(ServerSocketThread thread, ServerSocket socket);
    void onTimeoutAccept(ServerSocketThread thread, ServerSocket serverSocket);
    void onAcceptedSocket(ServerSocketThread thread, ServerSocket serverSocket, Socket socket);
    void onExceptionServerSocketThread(ServerSocketThread thread, Exception e);


}
