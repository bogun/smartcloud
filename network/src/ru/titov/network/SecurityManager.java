package ru.titov.network;

/**
 * Created by valentintitov on 26.06.17.
 */
public interface SecurityManager {

    void init();
    String getToken(String login, String password);
    boolean isTokenValid(String token);
    String getLoginBy(String token);
    void dispose();

}
