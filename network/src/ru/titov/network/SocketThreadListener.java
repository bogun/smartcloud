package ru.titov.network;

import java.io.Serializable;
import java.net.Socket;

/**
 * Created by valentintitov on 29.06.17.
 */
public interface SocketThreadListener {

    void onStartSocketThread(SocketThread socketThread);
    void onStopSocketThread(SocketThread socketThread);

    void onReadySocketThread(SocketThread socketThread, Socket socket);
    void onReceiveData(SocketThread socketThread, Socket socket, Serializable responseData);

    void onExceptionSocketThread(SocketThread socketThread, Socket socket, Exception e);
}
