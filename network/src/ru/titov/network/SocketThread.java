package ru.titov.network;

import java.io.*;
import java.net.Socket;

/**
 * Created by valentintitov on 29.06.17.
 */
public class SocketThread extends Thread {
    SocketThreadListener eventListener;
    Socket socket;
    ObjectOutputStream out;

    public SocketThread(SocketThreadListener listener, String name, Socket socket) {
        super(name);
        this.eventListener = listener;
        this.socket = socket;
        start();
    }

    @Override
    public void run() {
        eventListener.onStartSocketThread(this);
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            eventListener.onReadySocketThread(this,socket);
            while (!interrupted()) {
                Serializable data = (Serializable)in.readObject();
                eventListener.onReceiveData(this,socket,data);
            }
        } catch(IOException e) {
            eventListener.onExceptionSocketThread(this,socket,e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                eventListener.onExceptionSocketThread(this,socket,e);
            }
            eventListener.onStopSocketThread(this);
        }
    }

    public synchronized void sendData(Serializable data) {
        try {
            out.writeObject(data);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void close() {
        interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            eventListener.onExceptionSocketThread(this, socket,e);
        }
    }


}
