package ru.titov.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * Created by valentintitov on 26.06.17.
 */
public class ServerSocketThread extends Thread {

    private final int port;
    private final int timeout;
    private final ServerSocketThreadListener eventListener;

    public ServerSocketThread(ServerSocketThreadListener eventListener,
                              String name,
                              int port,
                              int timeout) {
        super(name);
        this.port = port;
        this.timeout = timeout;
        this.eventListener = eventListener;
        start();
    }

    @Override
    public void run() {
        eventListener.onStartServerSocketThread(this);
        try (ServerSocket serverSocket = new ServerSocket(port)){
            serverSocket.setSoTimeout(timeout);
            eventListener.onReadyServerSocketThread(this,serverSocket);
            while(!interrupted()) {
                Socket socket;
                try{
                    socket = serverSocket.accept();
                } catch (SocketTimeoutException e) {
                    eventListener.onTimeoutAccept(this,serverSocket);
                    continue;
                }
                eventListener.onAcceptedSocket(this,serverSocket,socket);
            }
        } catch (IOException e) {
            eventListener.onExceptionServerSocketThread(this,e);
        } finally {
            eventListener.onStopServerSocketThread(this);
        }
    }
}
