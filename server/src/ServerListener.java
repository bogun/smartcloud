public interface ServerListener {

    void onServerLog(Server server, String msg);
    void createNewSandbox(String name);
    void createNewFile(String sandbox, String filename, String extension, byte[] content);
    void deleteFile(String sandbox, String filename, String extension);

}
