import ru.titov.common.SerialData;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;


public class ServerManager implements ServerListener {

    private final String CLOUD_PATH = "server/sandboxes";
    private final String DELIMETER = "/";
//    private final String CLOUD_PATH = "/Users/valentin/Documents/Learning/Java/JavaDeepLearning/sanboxes/";
    final private int PORT = 8189;
    private final Server server = new Server(this, new SQLSecurityManager());

    public static void main(String[] args) {
        ServerManager manager = new ServerManager();
        manager.startListening();
    }

    private void startListening() {
        server.startListening(PORT);
    }

    @Override
    public void onServerLog(Server chatServer, String msg) {
        System.out.println(msg);
    }

    @Override
    public void createNewSandbox(String name) {
        //TODO: call from registration block
        File cloudDir = new File(CLOUD_PATH + DELIMETER + name);
        if (!cloudDir.exists()) {
            cloudDir.mkdirs();
        }
    }

    @Override
    public void createNewFile(String sandbox, String filename, String extension, byte[] content) {
        // TODO: 05.11.2017 Repair to real filepath
        String path = CLOUD_PATH + DELIMETER + sandbox + DELIMETER + filename + "." + extension;
        try {
            Path file = Paths.get(path);
            Files.write(file,content);
        } catch (IOException e) {
            e.printStackTrace();
            new RuntimeException(e);
        }
    }

    @Override
    public void deleteFile(String sandbox, String filename, String extension) {
        // TODO: 05.11.2017 Repair to real filepath
        String path = CLOUD_PATH + DELIMETER + sandbox + DELIMETER + filename + "." + extension;
        try {
            Path file = Paths.get(path);
            if (Files.deleteIfExists(file)) {
                System.out.println("Successfully deleted file by path: " + path);
            } else {
                System.out.println("Can't delete file by path: " + path);
            }
        } catch (IOException e) {
            e.printStackTrace();
            new RuntimeException(e);
        }
    }
}
