import ru.titov.common.SerialAuth;
import ru.titov.common.SerialData;
import ru.titov.network.SecurityManager;
import ru.titov.network.ServerSocketThread;
import ru.titov.network.ServerSocketThreadListener;
import ru.titov.network.SocketThread;
import ru.titov.network.SocketThreadListener;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

public class Server implements ServerSocketThreadListener,SocketThreadListener {

    ServerListener listener;
    private ServerSocketThread serverSocketThread;
    private final SecurityManager securityManager;
    private final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss: ");
    private final int TIMEOUT = 10000;
    private final Vector<SocketThread> clients = new Vector<>();


    public Server(ServerListener listener,
                  SecurityManager securityManager) {
        this.listener = listener;
        this.securityManager = securityManager;
    }


    public void startListening(int port) {
        if (serverSocketThread != null && serverSocketThread.isAlive()) {
            putLog("Server already started");
        } else {
            serverSocketThread = new ServerSocketThread(this,"ServerSocketThread", port, TIMEOUT);
        }
        securityManager.init();
    }

    public void stopListening() {
        if (serverSocketThread == null || !serverSocketThread.isAlive()) {
            putLog("Server isn't started");
            return;
        } else {
            serverSocketThread.interrupt();
            securityManager.dispose();
        }
    }


    /** Server socket thread intarface */
    @Override
    public void onStartServerSocketThread(ServerSocketThread thread) {
        System.out.println("onStartServerSocketThread");
    }

    @Override
    public void onStopServerSocketThread(ServerSocketThread thread) {
        System.out.println("onStopServerSocketThread");
    }

    @Override
    public void onReadyServerSocketThread(ServerSocketThread thread, ServerSocket socket) {
        System.out.println("onReadyServerSocketThread");
    }

    @Override
    public void onTimeoutAccept(ServerSocketThread thread, ServerSocket serverSocket) {
        System.out.println("onTimeoutAccept");
    }

    @Override
    public void onAcceptedSocket(ServerSocketThread thread, ServerSocket serverSocket, Socket socket) {
        System.out.println("Client connected: " + socket);
        String threadName = "Socket thread: " + socket.getInetAddress() + ":" + socket.getPort();
        new ClientSocketThread(this, threadName, socket) ;
    }

    @Override
    public void onExceptionServerSocketThread(ServerSocketThread thread, Exception e) {
        System.out.println("onExceptionServerSocketThread, " + e.getLocalizedMessage());
    }



    /** Socket Thread interface */
    @Override
    public void onStartSocketThread(SocketThread socketThread) {

    }

    @Override
    public void onStopSocketThread(SocketThread socketThread) {

    }

    @Override
    public void onReadySocketThread(SocketThread socketThread, Socket socket) {

    }

    @Override
    public void onReceiveData(SocketThread socketThread, Socket socket, Serializable responseData) {
        ClientSocketThread client = (ClientSocketThread) socketThread;
        if (client.isAuthorized()) {
            handleAuthorizedClient(client,(SerialData) responseData);
        } else {
            handleNonAuthorizedClient(client,(SerialAuth)responseData);
        }
    }

    private void handleNonAuthorizedClient(ClientSocketThread newClient, SerialAuth auth) {
        String login = auth.getLogin();
        String password = auth.getPassword();
        // TODO: 08.10.2017 Check if user exist, and if true, check number of authenticated devices. If less than something then generate token for this client. Client must save token and use it next time
        String token = securityManager.getToken(login,password);
        if (token == null) {
            newClient.authError();
            return;
        }
        //TODO: call from registration block
        listener.createNewSandbox(login);
        newClient.authorizedAccept(token);
        System.out.println(login + " successfully authorized");
    }


    private void handleAuthorizedClient(ClientSocketThread client, SerialData data) {
        System.out.println("User request send some data");
        String login = securityManager.getLoginBy(data.getToken());
        if (login != null) {
            switch (data.getAction()) {
                case CREATE:
                    listener.createNewFile(login,data.getFilename(),data.getExtension(),data.getContent());break;
                case DELETE:
                    listener.deleteFile(login,data.getFilename(),data.getExtension());break;
            }
        }
    }

    public ClientSocketThread getClientByToken(String token) {
        final int size = clients.size();
        for (int i = 0; i < size; i++) {
            ClientSocketThread client = (ClientSocketThread)clients.get(i);
            if (!client.isAuthorized()) {continue;}
            if (client.getToken().equals(token)) {return  client;}
        }
        return null;
    }

    @Override
    public void onExceptionSocketThread(SocketThread socketThread, Socket socket, Exception e) {
        System.out.println("Exception Socket Thread on Server: " + e.getMessage());
    }



    private synchronized void putLog(String str) {
        String msg = dateFormat.format(System.currentTimeMillis()) + " " + Thread.currentThread().getName() + " " + str;
        listener.onServerLog(this,msg);
    }
}
