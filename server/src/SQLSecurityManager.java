import ru.titov.network.SecurityManager;

import java.sql.*;

/**
 * Created by valentintitov on 26.06.17.
 */
public class SQLSecurityManager implements SecurityManager {

    /** Constants */
    /* Table Fields */
    final private String TOKEN = "token";
    final private String LOGIN = "login";
    /** Class Fields */
    private Connection connection;



    @Override
    public void init() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:smart_cloud.sqlite");
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getToken(String login, String password) {
        try {
            String sql = "SELECT token FROM users JOIN user_token WHERE users.login = ? AND password = ? AND users.id = user_token.user_id";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1,login);
            statement.setString(2,password);
            ResultSet result =  statement.executeQuery();
            if (result.next()) {
                return result.getString(TOKEN);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isTokenValid(String token) {
        if (getLoginBy(token) == null) { return false; }
        return true;
    }

    @Override
    public String getLoginBy(String token) {
        try {
            String sql = "SELECT login FROM users JOIN user_token WHERE user_token.token = ? AND users.id = user_token.user_id";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1,token);
            ResultSet result =  statement.executeQuery();
            if (result.next()) {
                return result.getString(LOGIN);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void dispose() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
