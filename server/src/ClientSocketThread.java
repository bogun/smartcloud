import ru.titov.common.Messages;
import ru.titov.common.SerialAuth;
import ru.titov.network.SocketThread;
import ru.titov.network.SocketThreadListener;

import java.net.Socket;

public class ClientSocketThread extends SocketThread {

    private boolean isAuthorized;
    private String token;

    public ClientSocketThread(SocketThreadListener listener,
                              String name,
                              Socket socket) {
        super(listener, name, socket);
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    void authError() {
        sendData(new SerialAuth("The login or password is incorrect",false));
    }

    void authorizedAccept(String token) {
        this.isAuthorized = true;
        sendData(new SerialAuth(token,true));
    }

    public String getToken() {
        return token;
    }

}
