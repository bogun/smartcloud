package ru.titov.client.core;

import javafx.application.Platform;
import ru.titov.client.gui.settings.Settings;
import ru.titov.common.Serial;
import ru.titov.common.SerialAuth;
import ru.titov.common.SerialData;
import ru.titov.common.SyncAction;
import ru.titov.network.SocketThread;
import ru.titov.network.SocketThreadListener;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import java.io.*;
import java.net.Socket;
import java.util.Timer;

public class ClientManager implements Thread.UncaughtExceptionHandler, SocketThreadListener {

    final private String LOCAL_FOLDER_PATH = "client/local_resources";

    final private String IP_ADDRESS = "127.0.0.1";
    final private int PORT = 8189;
    private SocketThread socketThread;

    private Timer timer;
    private boolean isConnect;
    private String token;
    private String lastHash;

    private LoginListener loginListener;


    public ClientManager() {
        connect();
    }

    public void setLoginListener(LoginListener listener) {
        this.loginListener = listener;
    }

    public void login(String login, String password) {
        if (isConnect == true) {
            SerialAuth auth = new SerialAuth(login,password);
            socketThread.sendData(auth);
        } else {
            loginListener.wrongLogged("No connection with server");
        }


//        //TODO: Recover last hash of observe folder from local db or properties.
//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                tick();
//            }
//        }, 0,5000);
    }

    void tick() {
        File dir = new File(LOCAL_FOLDER_PATH);
        if (!dir.exists()) {
            new RuntimeException("Directory is not exist");
        }
        String aHash = new CheckSum().calcMD5HashForDir(dir,false);
        if (!aHash.equals(lastHash)) {
            System.out.println("Hash changed");
            System.out.println(aHash);
        }
        lastHash = aHash;
    }



    private void connect() {
        try {
            Socket socket = new Socket(IP_ADDRESS, PORT);
            socketThread = new SocketThread(this, "SocketThread", socket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void disconnect() {
        System.out.println("Called disconnect");
        socketThread.close();
    }

    private void testSync() {
        String fileName = "humans";
        String extension = "txt";
        Path file = Paths.get(LOCAL_FOLDER_PATH + "/"+fileName+"."+extension);
        try {
            byte[] content = Files.readAllBytes(file);
            SerialData data = new SerialData(token,fileName,extension,content,SyncAction.CREATE);
            socketThread.sendData(data);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /** SocketThreadListener */
    @Override
    public void onStartSocketThread(SocketThread socketThread) {
        System.out.println("onStartSocketThread");
    }

    @Override
    public void onStopSocketThread(SocketThread socketThread) {
        System.out.println("onStopSocketThread");
    }

    @Override
    public void onReadySocketThread(SocketThread socketThread, Socket socket) {
        System.out.println("onReadySocketThread");
        isConnect = true;
    }

    @Override
    public void onReceiveData(SocketThread socketThread, Socket socket, Serializable responseData) {
        Serial data = (Serial) responseData;
        Class dataClass = data.getClass();
        if (dataClass.equals(SerialAuth.class)) {
            handleAuth((SerialAuth)data);
        } else if (dataClass.equals(SerialData.class)) {
            handleReceiveFile((SerialData)data);
        }
    }

    private void handleAuth(SerialAuth auth) {
        Platform.runLater(() -> {
            if(auth.isAuthAccept()) {
                token = auth.getToken();
                System.out.println("Client: Successfully authorized");
                loginListener.logged();
            } else {
                String errMessage = auth.getErrorMessage();
                loginListener.wrongLogged(errMessage);
            }
        });
    }

    private void handleReceiveFile(SerialData data) {
        if(token == data.getToken()) {
            String fileName = data.getFilename();
            String extension = data.getExtension();
            SyncAction action = data.getAction();
            // TODO: 05.11.2017 add file name to local path and create full path to file
            String fullName = fileName + "." + extension;
            Path localPath = Settings.getCloudFolderPath();
            switch (action) {
                case CREATE:
                    byte[] bytes = data.getContent();
                    createFile(localPath, bytes);
                    break;
                case DELETE:
                    deleteFile(localPath);
                    break;
            }
        } else {
            System.out.println("Invalid token");
        }
    }

    private void createFile(Path path, byte[] content) {

    }

    private void deleteFile(Path path) {

    }


    @Override
    public void onExceptionSocketThread(SocketThread socketThread, Socket socket, Exception e) {
        System.out.println("onExceptionSocketThread");
    }

    /** Thread.UncaughtExceptionHandler */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.out.println("uncaughtException. Exception: " + e.getLocalizedMessage());
    }
}
