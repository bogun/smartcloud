package ru.titov.client.core;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;
import java.util.*;


public class CheckSum {

    public String calcMD5HashForDir(File dirToHash, boolean includeHiddenFiles) {

        assert (dirToHash.isDirectory());
        Vector<FileInputStream> fileStreams = new Vector<FileInputStream>();

//        System.out.println("Found files for hashing:");
        collectInputStreams(dirToHash, fileStreams, includeHiddenFiles);

        SequenceInputStream seqStream =
                new SequenceInputStream(fileStreams.elements());

        try {
            String md5Hash = DigestUtils.md5Hex(seqStream);
            seqStream.close();
            return md5Hash;
        }
        catch (IOException e) {
            throw new RuntimeException("Error reading files to hash in "
                    + dirToHash.getAbsolutePath(), e);
        }

    }

    private void collectInputStreams(File dir,
                                     List<FileInputStream> foundStreams,
                                     boolean includeHiddenFiles) {

        File[] fileList = dir.listFiles();
        Arrays.sort(fileList,               // Need in reproducible order
                new Comparator<File>() {
                    public int compare(File f1, File f2) {
                        return f1.getName().compareTo(f2.getName());
                    }
                });

        for (File f : fileList) {
            if (!includeHiddenFiles && f.getName().startsWith(".")) {
                // Skip it
            }
            else if (f.isDirectory()) {
                collectInputStreams(f, foundStreams, includeHiddenFiles);
            }
            else {
                try {
                    //System.out.println("\t" + f.getAbsolutePath());
                    foundStreams.add(new FileInputStream(f));
                }
                catch (FileNotFoundException e) {
                    throw new AssertionError(e.getMessage()
                            + ": file should never not be found!");
                }
            }
        }

    }

}
