package ru.titov.client.core;

public interface LoginListener {
    void logged();
    void wrongLogged(String err);
}
