package ru.titov.client.gui;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.titov.client.core.ClientManager;
import ru.titov.client.gui.abstracts.Navigation;
import ru.titov.client.gui.login.LoginController;

public class SmartCloudApp extends Application {

    final public static double WINDOW_WIDTH = 250;
    final public static double WINDOW_HEIGHT = 180;
    final public static String APP_TITLE = "SCloud";


    @Override
    public void start(Stage primaryStage) throws Exception {
        navigation = new Navigation(primaryStage);
        primaryStage.setWidth(WINDOW_WIDTH);
        primaryStage.setHeight(WINDOW_HEIGHT);
        primaryStage.show();
        navigation.load(LoginController.URL_FXML).present();
    }



    public static void main(String[] args) {
        launch(args);
    }

    private static Navigation navigation;
    private static ClientManager clientManager = new ClientManager();


    public static Navigation getNavigation() {
        return navigation;
    }

    public static ClientManager getClientManager() {
        return clientManager;
    }
}
