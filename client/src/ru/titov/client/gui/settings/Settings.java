package ru.titov.client.gui.settings;

import java.io.*;
import java.nio.file.Path;
import java.util.Properties;

public class Settings {

    private final static String SETTINGS_FILE_NAME = "settings.properties";
    private final static String TOKEN = "token";
    private final static String CLOUD_FOLDER_PATH = "cloud_folder_path";

    private static Properties properties;

    static {
        try {
            File file = new File(SETTINGS_FILE_NAME);
            properties = new Properties();
            if (file.exists()) {
                FileInputStream fileInput = new FileInputStream(file);
                properties.load(fileInput);
                fileInput.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveToken() {

    }

    public static String getToken() {
        return null;
    }

    public static boolean isToken() {
        return false;
    }

    public static void saveCloudFolderPath(Path path) {
        properties.setProperty(CLOUD_FOLDER_PATH, path.toString());
        commit();
    }

    private static void commit() {
        try {
            File file = new File(SETTINGS_FILE_NAME);
            FileOutputStream fileOut = new FileOutputStream(file);
            properties.store(fileOut, "Settings");
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Path getCloudFolderPath() {
        return null;
    }
}
