package ru.titov.client.gui.login;

import javafx.fxml.FXML;

import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import ru.titov.client.core.LoginListener;
import ru.titov.client.core.ClientManager;
import ru.titov.client.gui.SmartCloudApp;
import ru.titov.client.gui.abstracts.BaseController;
import ru.titov.client.gui.choose.ChooseController;
import ru.titov.client.gui.finder.FinderController;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController extends BaseController implements LoginListener, Initializable {

    final private static String TITLE_FXML = "Authorization";
    public static final String URL_FXML = "login/login_screen.fxml";

/** GUI Elements */
    @FXML
    TextField loginField;
    @FXML
    PasswordField passField;
    @FXML
    VBox rootElement;
    @FXML
    Label errLabel;

    private ClientManager manager;

/** Constructor */
    public LoginController() {
        manager = SmartCloudApp.getClientManager();
        manager.setLoginListener(this);
    }


    public void tryToLogin() throws Exception {
        String login = loginField.getText();
        String password = passField.getText();
        errLabel.setText("");
        if (isValidLogin(login) && isValidPassword(password)) {
            manager.login(login,password);
        }
    }

    private boolean isValidLogin(String login) {
        return true;
    }

    private boolean isValidPassword(String password) {
        return true;
    }

    /** Client Listener */
    @Override
    public void logged() {
        showChooseScreen();
    }

    @Override
    public void wrongLogged(String err) {
        errLabel.setText(err);
    }


    public void showChooseScreen() {
        SmartCloudApp.getNavigation().load(ChooseController.URL_FXML).present();

    }


    @Override
    public Node getView() {
        return super.getView();
    }

    @Override
    public void setView(Node view) {
        super.setView(view);
    }

    @Override
    public void present() {
        super.present();
    }

    @Override
    public void viewWillAppear() {
        super.viewWillAppear();
        SmartCloudApp.getNavigation().setWindwoTitle(SmartCloudApp.APP_TITLE + " " + TITLE_FXML);
    }

    @Override
    public void viewDidAppear() {
        super.viewDidAppear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
