package ru.titov.client.gui.choose;


import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.layout.VBox;
import ru.titov.client.gui.NavigateListener;
import ru.titov.client.gui.SmartCloudApp;
import ru.titov.client.gui.abstracts.BaseController;
import ru.titov.client.gui.abstracts.Navigation;
import ru.titov.client.gui.finder.FinderController;

import java.net.URL;
import java.util.ResourceBundle;

public class ChooseController extends BaseController implements Initializable {

    public static final String URL_FXML = "choose/choose_screen.fxml";

/** GUI Elements */
@FXML
Label selectLabel;


    public void selectFolder() {
        showFinderScreen();
    }

    public void onDragDropped(DragEvent event) {
        System.out.println(event.getPickResult());
    }


    @Override
    public Node getView() {
        return super.getView();
    }

    @Override
    public void setView(Node view) {
        super.setView(view);
    }

    @Override
    public void present() {
        super.present();
    }

    @Override
    public void viewWillAppear() {
        super.viewWillAppear();
        SmartCloudApp.getNavigation().setWindwoTitle(SmartCloudApp.APP_TITLE);
    }

    @Override
    public void viewDidAppear() {
        super.viewDidAppear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        selectLabel.setOnAction
    }

    public void showFinderScreen() {
        Navigation navigator = SmartCloudApp.getNavigation();
        navigator.load(FinderController.URL_FXML).present();
        navigator.resize(650.0,400.0);
    }
}
