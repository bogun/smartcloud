package ru.titov.client.gui;

public interface NavigateListener {
    void onNavigate();
}
