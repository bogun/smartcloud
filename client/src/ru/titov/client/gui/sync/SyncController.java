package ru.titov.client.gui.sync;


import javafx.fxml.Initializable;
import javafx.scene.Node;
import ru.titov.client.gui.SmartCloudApp;
import ru.titov.client.gui.abstracts.BaseController;
import ru.titov.client.gui.login.LoginController;

import java.net.URL;
import java.util.ResourceBundle;

public class SyncController extends BaseController implements Initializable {

    public static final String URL_FXML = "sync/sync_screen.fxml";
    final private static String TITLE_FXML = "Synhronization";

/** GUI Elements */

    public void startSynchronization() {
        System.out.println("Begin files synchronization");
    }

    @Override
    public Node getView() {
        return super.getView();
    }

    @Override
    public void setView(Node view) {
        super.setView(view);
    }

    @Override
    public void present() {
        super.present();
    }

    @Override
    public void viewWillAppear() {
        super.viewWillAppear();
        SmartCloudApp.getNavigation().setWindwoTitle(SmartCloudApp.APP_TITLE + " " + TITLE_FXML);
    }

    @Override
    public void viewDidAppear() {
        super.viewDidAppear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        selectLabel.setOnAction
    }


}
