package ru.titov.client.gui.abstracts;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import ru.titov.client.gui.SmartCloudApp;

import java.util.ArrayList;
import java.util.List;

public class Navigation {

    private final Stage stage;
    private final Scene scene;
    private List<Controller> controllers = new ArrayList<>();

    public Navigation(Stage stage) {
        this.stage = stage;
        this.scene = new Scene(new Pane());
        stage.setScene(scene);
    }

    public Controller load(String sUrl) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(SmartCloudApp.class.getResource(sUrl));
            Node root = fxmlLoader.load();
            Controller controller = fxmlLoader.getController();
            controller.setView(root);
            return controller;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void present(Controller controller) {
        try {
            scene.setRoot((Parent)controller.getView());
            controllers.add(controller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resize(double width, double height) {
        stage.setWidth(width);
        stage.setHeight(height);
    }

    public void setWindwoTitle(String title) {
        stage.setTitle(title);
    }

    public  void back() {
        if (controllers.size() > 1) {
            controllers.remove(controllers.get(controllers.size()-1));
            scene.setRoot((Parent)controllers.get(controllers.size()-1).getView());
        }
    }

    public void clearHistory() {
        while (controllers.size() > 1) {
            controllers.remove(0);
        }
    }



}
