package ru.titov.client.gui.abstracts;

import javafx.scene.Node;
import ru.titov.client.gui.SmartCloudApp;

public class BaseController  implements Controller {

    private Node view;

    @Override
    public Node getView() {
        return view;
    }

    @Override
    public void setView(Node view) {
        this.view = view;
    }

    @Override
    public void present() {
        viewWillAppear();
        SmartCloudApp.getNavigation().present(this);
    }

    public void viewWillAppear() {

    }

    public void viewDidAppear() {

    }
}
