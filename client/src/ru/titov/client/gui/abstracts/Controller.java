package ru.titov.client.gui.abstracts;

import javafx.scene.Node;

public interface Controller {
    Node getView();
    void  setView(Node view);
    void present();
}
