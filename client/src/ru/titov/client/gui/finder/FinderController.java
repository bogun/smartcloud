package ru.titov.client.gui.finder;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import ru.titov.client.gui.SmartCloudApp;
import ru.titov.client.gui.abstracts.BaseController;
import ru.titov.client.gui.abstracts.Navigation;
import ru.titov.client.gui.settings.Settings;
import ru.titov.client.gui.sync.SyncController;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;


public class FinderController extends BaseController implements Initializable {

    /* Constants */
    private static final int KB = 1024;
    private static final int COL_TYPE_WIDTH = 30;
    private static final String COL_TYPE_TITLE = "T";
    private static final int COL_NAME_WIDTH = 150;
    private static final String COL_NAME_TITLE = "Name";
    private static final int COL_SIZE_WIDTH = 60;
    private static final String COL_SIZE_TITLE = "Size(Kb)";
    /* Pathes constants */
    public static final String URL_FXML = "finder/finder_screen.fxml";
    private static final String TITLE_FXML = "Choose root folder";
    private static final String PATH_TO_APP = "/Users/valentin/Documents/Learning/Java/JavaDeepLearning/GeekCloud/SmartCloud/";
    private static final String PATH_TO_ICONS = "client/src/ru/titov/client/gui/resources/";
    private static final String UP_IMG_NAME = "up.png";
    private static final String FILE_TYPE_IMG_NAME = "paper.png";
    private static final String FOLDER_TYPE_IMG_NAME = "folder.png";

    /* View elements */
    @FXML
    TableView<Path> tableView;


    private  Path currentPath;
    private ObservableList<Path> list;



    @Override
    public Node getView() {
        return super.getView();
    }

    @Override
    public void setView(Node view) {
        super.setView(view);
    }

    @Override
    public void present() {
        super.present();
    }

    @Override
    public void viewWillAppear() {
        super.viewWillAppear();
        SmartCloudApp.getNavigation().setWindwoTitle(SmartCloudApp.APP_TITLE + " " + TITLE_FXML);
    }

    @Override
    public void viewDidAppear() {
        super.viewDidAppear();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentPath = Paths.get(PATH_TO_APP);
        list = FXCollections.observableArrayList();
        resetList();
        TableColumn<Path, ImageView> colType = new TableColumn<>(COL_TYPE_TITLE);
        colType.setCellValueFactory(param -> {
            ImageView iv = null;
            try {
                Path path = param.getValue();
                if (isLevelUp(path)) {
                    iv = new ImageView(new File(PATH_TO_ICONS + UP_IMG_NAME).toURI().toURL().toString());
                } else if (!Files.isDirectory(path)) {
                    iv = new ImageView(new File(PATH_TO_ICONS + FILE_TYPE_IMG_NAME).toURI().toURL().toString());
                } else {
                    iv = new ImageView(new File(PATH_TO_ICONS + FOLDER_TYPE_IMG_NAME).toURI().toURL().toString());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return new SimpleObjectProperty<>(iv);
        });
        colType.setPrefWidth(COL_TYPE_WIDTH);
        TableColumn<Path, String> colName = new TableColumn<>(COL_NAME_TITLE);
        colName.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getFileName().toString()));
        colName.setPrefWidth(COL_NAME_WIDTH);
        colName.setStyle("-fx-alignment: CENTER-LEFT;");
        TableColumn<Path, String> colSize = new TableColumn<>(COL_SIZE_TITLE);
        colSize.setCellValueFactory(param -> {
            try {
                Path path = param.getValue();
                String value = "";
                if(!isDirectory(path)) {
                    value = String.valueOf(Files.size(path)/KB);
                }
                return new SimpleObjectProperty<>(value);
            } catch (IOException e) {
                e.printStackTrace();
                return new SimpleObjectProperty<>("");
            }
        });
        colSize.setPrefWidth(COL_SIZE_WIDTH);
        tableView.getColumns().addAll(colType, colName, colSize);
        tableView.setOnMousePressed(event -> {
            if(event.getClickCount() > 1) {
                Path path = tableView.getSelectionModel().getSelectedItem();
                String selectedPath = String.valueOf(path);
                if (isLevelUp(path)) {
                    System.out.println("Move to up level");
                    Path parent = currentPath.getParent();
                    if (parent != null) {
                        currentPath = parent;
                        updateTable();
                    }
                } else if (new File(selectedPath).isDirectory()) {
                    currentPath = Paths.get(selectedPath);
                    updateTable();
                } else {
                    System.out.println("Selection isn't a directory");
                }
            }
        });
        updateTable();
    }

    private void updateTable() {
        resetList();
        try {
            Files.newDirectoryStream(currentPath).forEach(p -> {
                if (!p.getFileName().toString().equals(".DS_Store")) {
                    list.add(p);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        tableView.setItems(list);
        tableView.refresh();
    }


    /* Button actions */
    public void onCancel() {
        Navigation navigator = SmartCloudApp.getNavigation();
        navigator.back();
        navigator.resize(SmartCloudApp.WINDOW_WIDTH,SmartCloudApp.WINDOW_HEIGHT);
    }

    public void onSelect() {
        Path path = tableView.getSelectionModel().getSelectedItem();
        if (isDirectory(path)) {
            currentPath = path;
            Settings.saveCloudFolderPath(currentPath);
            showStartSyncScreen();
        } else {
            System.out.println("The selection should be directory. Please, try again");
        }
    }

    public void showStartSyncScreen() {
        Navigation navigator = SmartCloudApp.getNavigation();
        navigator.load(SyncController.URL_FXML).present();
        navigator.resize(SmartCloudApp.WINDOW_WIDTH,SmartCloudApp.WINDOW_HEIGHT);
    }

    private void resetList() {
        list.removeAll(list);
        Path up = Paths.get("..");
        list.add(up);
    }

    private boolean isLevelUp(Path path) {
        String lastComponent = path.getFileName().toString();
        return lastComponent.equals("..");
    }

    private boolean isDirectory(Path path) {
       return path.toFile().isDirectory();
    }
}
