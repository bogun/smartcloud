package ru.titov.common;


public class SerialAuth extends Serial {
    private String login;
    private String password;
    private String token;
    private String errorMessage;
    private boolean isAuthAccept;

    public SerialAuth(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public SerialAuth(String msg, boolean isAuthAccept) {
        this.isAuthAccept = isAuthAccept;
        if (isAuthAccept) {
            this.token = msg;
        } else {
            this.errorMessage = msg;
        }
    }


    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isAuthAccept() {
        return isAuthAccept;
    }
}
