package ru.titov.common;

/**
 * Created by valentintitov on 03.07.17.
 */
public class Messages {

    public static final String DELIMITER =          ";";
    public static final String AUTH_REQUEST =       "/auth_request";
    public static final String AUTH_ACCEPT =        "/auth_accept";
    public static final String AUTH_ERROR =         "/auth_error";
    public static final String SYNC_REQUEST =       "/sync_request";

    public static final String USERS_LIST =         "/user_list";
    public static final String RECONNECT =          "/reconnect";
    public static final String BROADCAST =          "/bcast";
    public static final String MSG_FORMAT_ERROR =   "/msg_format_error";


/** Authorization */
    public static String getAuthRequest(String login, String password){
        return AUTH_REQUEST + DELIMITER + login + DELIMITER + password;
    }
    public static String getAuthAccept(String token) {
        return AUTH_ACCEPT + DELIMITER + token;
    }

 // AUTH_ERROR time message
    public static String getAuthError(){
     return AUTH_ERROR;
 }

    public static String getReconnect() {
        return RECONNECT;
    }

/** Synchronization file */
    public static String getSyncRequest(String token,
                                        String filename,
                                        String extension) {
        return SYNC_REQUEST + DELIMITER + token + DELIMITER + filename + DELIMITER + extension;
    }

/**  Message format error */
    public static String getMsgFormatError(String value) {
        return MSG_FORMAT_ERROR + DELIMITER + value;
    }
}