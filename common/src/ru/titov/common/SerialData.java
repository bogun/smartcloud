package ru.titov.common;

public class SerialData extends Serial {

    private String token;
    private String filename;
    private String extension;
    private byte[] content;
    private SyncAction action;

    public SerialData(String token, String filename, String extension, byte[] content, SyncAction action) {
        this.token = token;
        this.filename = filename;
        this.extension = extension;
        this.action = action;
        this.content = content;
    }

    public String getToken() {
        return token;
    }

    public String getFilename() {
        return filename;
    }

    public String getExtension() {
        return extension;
    }

    public SyncAction getAction() {
        return action;
    }

    public byte[] getContent() {
        return content;
    }
}
